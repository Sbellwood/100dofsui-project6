//
//  ContentView.swift
//  Animations
//
//  Created by Skyler Bellwood on 7/6/20.
//  Copyright © 2020 Skyler Bellwood. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    @State private var animationAmount: CGFloat = 1
    @State private var rotationAnimationAmount = 0.0
    @State private var enabled = false
    @State private var dragAmount = CGSize.zero
    @State private var isShowingRed = false
    let letters = Array("Hello SwiftUI")
    
    var body: some View {
        //        Button("Tap Me") {
        //            self.animationAmount += 1
        //        }
        //        .padding(50)
        //        .background(Color.red)
        //        .foregroundColor(.white)
        //        .clipShape(Circle())
        //        .overlay(
        //            Circle()
        //            .stroke(Color.red)
        //            .scaleEffect(animationAmount)
        //            .opacity(Double(2 - animationAmount))
        //            .animation(
        //                Animation.easeInOut(duration: 1)
        //                .repeatForever(autoreverses: false)
        //            )
        //        )
        //            .onAppear {
        //                self.animationAmount = 2
        //        }
        //        .scaleEffect(animationAmount)
        //        .animation(.default)
        //        .animation(.easeOut)
        //        .animation(.interpolatingSpring(stiffness: 50, damping: 1))
        //        .animation(.easeInOut(duration: 2))
        //        .animation(
        //            Animation.easeInOut(duration: 2)
        //            .delay(1)
        //        )
        //        .animation(
        //            Animation.easeInOut(duration: 1)
        //            .repeatCount(3, autoreverses: true)
        //        )
        //        .animation(
        //            Animation.easeInOut(duration: 1)
        //            .repeatForever(autoreverses: true)
        //        VStack {
        //            Stepper("Scale amount", value: $animationAmount.animation(), in: 1...10)
        //
        //            Spacer()
        //
        //            Button("Tap Me") {
        //                self.animationAmount += 1
        //            }
        //            .padding(40)
        //            .background(Color.red)
        //            .foregroundColor(Color.white)
        //            .clipShape(Circle())
        //            .scaleEffect(animationAmount)
        //        }
        
        //        Button("Tap Me") {
        //            withAnimation(.default) {
        //                self.rotationAnimationAmount += 360
        //            }
        //        }
        //        .padding(50)
        //        .background(Color.red)
        //        .foregroundColor(.white)
        //        .clipShape(Circle())
        //        .rotation3DEffect(.degrees(rotationAnimationAmount), axis: (x: 0, y: 0, z: 1))
        
        //        VStack {
        //            Button("Tap Me") {
        //                // do nothing
        //            }
        //            .background(Color.blue)
        //            .frame(width: 200, height: 200)
        //            .foregroundColor(.white)
        //
        //            Button("Tap Me") {
        //                // do nothing
        //            }
        //            .frame(width: 200, height: 200)
        //            .background(Color.blue)
        //            .foregroundColor(.white)
        //        }
        
        //        Button("Tap Me") {
        //            self.enabled.toggle()
        //        }
        //        .frame(width: 200, height: 200)
        //        .background(enabled ? Color.blue : Color.red)
        //        .animation(.default)
        //        .foregroundColor(.white)
        //        .clipShape(RoundedRectangle(cornerRadius: enabled ? 60 : 0))
        //        .animation(.interpolatingSpring(stiffness: 5, damping: 1))
        
        //        LinearGradient(gradient: Gradient(colors: [.yellow, .red]), startPoint: .topLeading, endPoint: .bottomTrailing)
        //            .frame(width: 300, height: 200)
        //            .clipShape(RoundedRectangle(cornerRadius: 10))
        //            .offset(dragAmount)
        //            .gesture(
        //                DragGesture()
        //                    .onChanged { self.dragAmount = $0.translation }
        //                    .onEnded { _ in
        //                        withAnimation(.spring()) {
        //                            self.dragAmount = .zero
        //                        }
        //                }
        //            )
        //            .animation(.spring())
        
//        HStack(spacing: 0) {
//            ForEach(0..<letters.count) { number in
//                Text(String(self.letters[number]))
//                    .padding(5)
//                    .font(.title)
//                    .background(self.enabled ? Color.blue : Color.green)
//                    .offset(self.dragAmount)
//                    .animation(Animation.default.delay(Double(number) / 20))
//            }
//        }
//        .gesture(
//            DragGesture()
//                .onChanged { self.dragAmount = $0.translation }
//                .onEnded { _ in
//                    self.dragAmount = .zero
//                    self.enabled.toggle()
//            }
//        )
        
        VStack {
            Button("Tap Me") {
                withAnimation {
                    self.isShowingRed.toggle()
                }
            }
            
            if isShowingRed {
                Rectangle()
                    .fill(Color.red)
                    .frame(width: 200, height: 200)
//                    .transition(.asymmetric(insertion: .scale, removal: .opacity))
//                    .transition(.scale)
                    .transition(.pivot)
            }
        }
        
    }
}

struct CornerRotateModifier: ViewModifier {
    let amount: Double
    let anchor: UnitPoint
    
    func body(content: Content) -> some View {
        content.rotationEffect(.degrees(amount), anchor: anchor).clipped()
    }
}

extension AnyTransition {
    static var pivot: AnyTransition {
        .modifier(active: CornerRotateModifier(amount: -90, anchor: .topLeading), identity: CornerRotateModifier(amount: 0, anchor: .topLeading))
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
